import numpy as np
import pandas as pd
import msgpack
import glob
import tensorflow as tf
from tensorflow.python.ops import control_flow_ops
from tqdm import tqdm

###################################################
# This code works in conjunction with the midi-manipulation file that is not of my own

import midi_manipulation

def get_songs(path):
    files = glob.glob('{}/*.mid*'.format(path))
    songs = []
    for f in tqdm(files):
        try:
            song = np.array(midi_manipulation.midiToNoteStateMatrix(f))
            if np.array(song).shape[0] > 50:
                songs.append(song)
        except Exception as e:
            raise e
    return songs

songs = get_songs('folder') #Songs must be converted into msgpack
print: "{} songs processed".format(len(songs))
###################################################

# The parameters of the network

lowest_note = midi_manipulation.lowerBound #the index of the lowest note on the piano roll
highest_note = midi_manipulation.upperBound #the index of the highest note on the piano roll
note_range = highest_note-lowest_note #the note range

num_timesteps  = 15 #number of timesteps per call
n_visible      = 2*note_range*num_timesteps #size of visible layer
n_hidden       = 50 #size of hidden layer

num_epochs = 200 #number of training epochs
batch_size = 100 #number of examples
lr         = tf.constant(0.005, tf.float32) #learning rate


# var

x  = tf.placeholder(tf.float32, [None, n_visible], name="x") #dataholder
W  = tf.Variable(tf.random_normal([n_visible, n_hidden], 0.01), name="W") #edge weight matrix
bh = tf.Variable(tf.zeros([1, n_hidden],  tf.float32, name="bh")) #bias for hidden layer
bv = tf.Variable(tf.zeros([1, n_visible],  tf.float32, name="bv")) #bias for visible layer



#helper functions
def sample(probs):
    #samples vectors into input vector
    return tf.floor(probs + tf.random_uniform(tf.shape(probs), 0, 1))

def gibbs_sample(k):
    #runs gibbs chain
    def gibbs_step(count, k, xk):
        hk = sample(tf.sigmoid(tf.matmul(xk, W) + bh)) #sample hidden values
        xk = sample(tf.sigmoid(tf.matmul(hk, tf.transpose(W)) + bv)) #sample visible values
        return count+1, k, xk

    #run gibbs steps
    ct = tf.constant(0) #counter
    [_, _, x_sample] = control_flow_ops.while_loop(lambda count, num_iter, *args: count < num_iter,
                                         gibbs_step, [ct, tf.constant(k), x])
    x_sample = tf.stop_gradient(x_sample)
    return x_sample


#using some equations in order to train
x_sample = gibbs_sample(1)
#sample hidden nodes
h = sample(tf.sigmoid(tf.matmul(x, W) + bh))
h_sample = sample(tf.sigmoid(tf.matmul(x_sample, W) + bh))

#updating values
size_bt = tf.cast(tf.shape(x)[0], tf.float32)
W_adder  = tf.mul(lr/size_bt, tf.sub(tf.matmul(tf.transpose(x), h), tf.matmul(tf.transpose(x_sample), h_sample)))
bv_adder = tf.mul(lr/size_bt, tf.reduce_sum(tf.sub(x, x_sample), 0, True))
bh_adder = tf.mul(lr/size_bt, tf.reduce_sum(tf.sub(h, h_sample), 0, True))
updt = [W.assign_add(W_adder), bv.assign_add(bv_adder), bh.assign_add(bh_adder)]





with tf.Session() as sess:
    #trains model and initializes variables of the model

    init = tf.global_variables_initializer()
    sess.run(init)
    #run training through epochs
    for epoch in tqdm(range(num_epochs)):
        for song in songs:

            song = np.array(song)
            song = song[:np.floor(song.shape[0]/num_timesteps)*num_timesteps]
            song = np.reshape(song, [song.shape[0]/num_timesteps, song.shape[1]*num_timesteps])
            for i in range(1, len(song), batch_size):
                tr_x = song[i:i+batch_size]
                sess.run(updt, feed_dict={x: tr_x})


    #gibbs chain with values set to 0
    sample = gibbs_sample(1).eval(session=sess, feed_dict={x: np.zeros((10, n_visible))})
    for i in range(sample.shape[0]):
        if not any(sample[i,:]):
            continue
        S = np.reshape(sample[i,:], (num_timesteps, 2*note_range))
        midi_manipulation.noteStateMatrixToMidi(S, "generated_chord_{}".format(i))